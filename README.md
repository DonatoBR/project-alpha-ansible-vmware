# project-alpha-ansible-vmware

## Comment ca marche ?

Le playbook à pour but de deployer des VM grace à ansible. Pour cela nous avons 3 fichier, le playbook, des vars et un vault.

Pour configurer les paramètre que nous voulons attribuer à une VM nous modifions le fichier vars_multiple.yml, la description des variable se trouve directement dans le playbook.

Pour pouvoir deployer sur le vCenter nous utilisons un fichier vault qui permet de hasher les variables sensible.

## Attention !!!!!
une fois le fichier de variable modifier il ne faut jamais le pousser sur la master, le code à simplement pour but de deployer des VMs via des templates. Il ne fonctionne pas comme terraform !!

## jouer le deploy-vms.yml

```
ansible-galaxy install -r requirements.yml
ansible-playbook deploy-vms.yml --ask-vault-pass
```

